#include "../resource.h"

#include <BSEngine.h>
#include <Scene/PlayScene.h>

using namespace BSEngine;

int main()
{
#ifdef _DEBUG
	// test
	argc = 5;
	argv[1] = TEXT("COM7");
	argv[2] = TEXT("asdasd");
	argv[3] = TEXT("1");
	argv[4] = TEXT("1.0");
#endif

	HANDLE mutex = CreateMutex(NULL, false, TEXT("BlUE Voltex Play"));
	
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		return 0;
	}

#ifdef _DEBUG
	if (argc < 5)
#else
	if (!lstrlen(lpszCmdLine))
#endif
	{
		ShellExecute(NULL, TEXT("open"), TEXT("BlUE Voltex.exe"), NULL, NULL, SW_SHOWNORMAL);

		return 0;
	}
	
	_tstring portName;
	_tstring musicName;
	int difficulty;
	float speed;
	
#ifdef _DEBUG
	portName = argv[1];
	musicName = argv[2];
	difficulty = _ttoi(argv[3]);
	speed = (float)_ttof(argv[4]);
#else
	_tstringstream parameterStream(lpszCmdLine);
	_tstring token;

	getline(parameterStream, token, TEXT(' '));
	portName = token;

	getline(parameterStream, token, TEXT(' '));
	musicName = token;

	getline(parameterStream, token, TEXT(' '));
	difficulty = _ttoi(token.c_str());

	getline(parameterStream, token, TEXT(' '));
	speed = (float)_ttof(token.c_str());
#endif

	HINSTANCE instance = GetModuleHandle(NULL);

	BSEngine::Engine engine;

	engine.Initialize(new PlayScene(TEXT("play"), portName, musicName, difficulty, speed),
					  800, 600,
					  TEXT("BlUE Voltex"), TEXT("BlUE Voltex"),
					  LoadIcon(instance, MAKEINTRESOURCE(IDI_ICON)),
					  LoadCursor(instance, MAKEINTRESOURCE(IDC_ARROW)),
					  false);
	engine.MainLoop();
	engine.Release();

	ReleaseMutex(mutex);

	return 0;
}