#include "ComboObject.h"

ComboObject::ComboObject(_tstring _name) : Object(_name)
{
	combo = 0;
}

ComboObject::~ComboObject() {}

void ComboObject::Render()
{
	char _combo[5];
	
	_itoa_s(combo, _combo, 5, 10);

	int count = strlen(_combo);

	for (int i = 0; i < count; ++i)
	{
		LoadTexture(TEXT("image/combo_") + to_tstring(_combo[i] - TEXT('0')) + TEXT(".png"));
		SetPosition(((800.0f - size.x) / 2.0f) +
			((i - ((float)(count - 1) / 2)) * size.x),
					400.0f);
		Object::Render();
	}
}