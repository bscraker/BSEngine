#include "BPMManagerObject.h"

BPMManagerObject::BPMManagerObject(_tstring _name) : Object(_name) {}

BPMManagerObject::~BPMManagerObject() {}

void BPMManagerObject::LoadData(_tstring datas)
{
	_tstringstream datasStream(datas);
	_tstring data;

	while (getline(datasStream, data, TEXT('\n')))
	{
		_tstringstream dataStream(data);
		_tstring token;

		BPMObject* _bpm = new BPMObject(TEXT(""));

		getline(dataStream, token, TEXT('|'));
		_bpm->order = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_bpm->beatNumerator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_bpm->beatDenominator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_bpm->bpm = (float)_ttof(token.c_str());

		bpms.push_back(_bpm);
	}
}

void BPMManagerObject::ConvertTime(vector<NoteObject*>* notes, float startBPM)
{
	for each (NoteObject* note in *notes)
	{
		float time = 0;
		int lastOrder = 0;
		float lastBPM = startBPM;

		for each (BPMObject* bpm in bpms)
		{
			if (bpm->order < note->order)
			{
				time += ((bpm->order - lastOrder) +
					((float)bpm->beatNumerator / bpm->beatDenominator)) *
					(4 * 60000 / lastBPM);
				lastOrder = (bpm->order + 1);
				lastBPM = bpm->bpm;
			}
			else
			{
				break;
			}
		}

		time += ((note->order - lastOrder) +
			((float)note->beatNumerator / note->beatDenominator)) *
			(4 * 60000 / lastBPM);
		note->time = (ULONG)time;
	}
}

void BPMManagerObject::ConvertTime(vector<SpeedObject*>* speeds, float startBPM)
{
	for each (SpeedObject* speed in *speeds)
	{
		float time = 0;
		int lastOrder = 0;
		float lastBPM = startBPM;

		for each (BPMObject* bpm in bpms)
		{
			if (bpm->order < speed->order)
			{
				time += ((bpm->order - lastOrder) +
					((float)bpm->beatNumerator / bpm->beatDenominator)) *
					(4 * 60000 / lastBPM);
				lastOrder = (bpm->order + 1);
				lastBPM = bpm->bpm;
			}
			else
			{
				break;
			}
		}

		time += ((speed->order - lastOrder) +
			((float)speed->beatNumerator / speed->beatDenominator)) *
			(4 * 60000 / lastBPM);
		speed->time = (ULONG)time;
	}
}

float BPMManagerObject::GetBPM(ULONG _time, float startBPM)
{
	float time = 0;
	int lastOrder = 0;
	float lastBPM = startBPM;

	for each (BPMObject* bpm in bpms)
	{
		time += ((bpm->order - lastOrder) +
			((float)bpm->beatNumerator / bpm->beatDenominator)) *
			(4 * 60000 / lastBPM);

		if (time >= _time)
		{
			break;
		}

		lastOrder = (bpm->order + 1);
		lastBPM = bpm->bpm;
	}

	return lastBPM;
}
