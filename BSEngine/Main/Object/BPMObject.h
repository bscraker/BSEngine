#pragma once

#include <Object.h>

using namespace BSEngine;

class BPMObject : public Object
{
public:
	BPMObject(_tstring _name);
	~BPMObject();

	virtual void Render() override {};

	int order;
	int beatNumerator;
	int beatDenominator;
	float bpm;
};