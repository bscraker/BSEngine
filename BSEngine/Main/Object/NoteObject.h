#pragma once

#include <Object.h>

using namespace BSEngine;

class NoteObject : public Object
{
public:
	NoteObject(_tstring _name);
	~NoteObject();

	int order;
	int beatNumerator;
	int beatDenominator;
	int positionNumerator;
	int positionDenominator;
	bool isLongNote;

	ULONG time;

	float positionRate;

	bool isChecked;
	bool isMissing;
};