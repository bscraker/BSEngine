#pragma once

#include <Object.h>

using namespace BSEngine;

class SpeedObject : public Object
{
public:
	SpeedObject(_tstring _name);
	~SpeedObject();

	virtual void Render() override {};

	int order;
	int beatNumerator;
	int beatDenominator;
	float speed;

	ULONG time;
};