#pragma once

#include <Object.h>

using namespace BSEngine;

class JudgementObject : public Object
{
public:
	JudgementObject(_tstring _name);
	~JudgementObject();

	virtual void Update() override;

	void Judge(UINT judge, float _bpm);

private:
	ULONG tick;
	float bpm;
};