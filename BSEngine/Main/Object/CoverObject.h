#pragma once

#include <Object.h>

using namespace BSEngine;

class CoverObject : public Object
{
public:
	CoverObject(_tstring _name);
	~CoverObject();

	virtual void Update() override;

	void Cover(bool isWhite, float _maxRate, ULONG _time);

private:
	ULONG tick;
	float maxRate;
	ULONG time;
};