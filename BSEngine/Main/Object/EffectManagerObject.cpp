#include "EffectManagerObject.h"

EffectManagerObject::EffectManagerObject(_tstring _name) : Object(_name) {}

EffectManagerObject::~EffectManagerObject() {}

void EffectManagerObject::Update()
{
	for each (EffectObject* effect in effects)
	{
		if (effect->visible)
		{
			effect->Update();
		}
	}
}

void EffectManagerObject::Render()
{
	for each (EffectObject* effect in effects)
	{
		if (effect->visible)
		{
			effect->Render();
		}
	}
}

void EffectManagerObject::CreateEffect(Object* xObject, Object* yObject, bool index, bool isLongNote, float bpm, ULONG delay)
{
	EffectObject* effect = NULL;

	for each (EffectObject* _effect in effects)
	{
		if (!_effect->visible)
		{
			effect = _effect;

			effect->visible = true;
		}
	}

	if (!effect)
	{
		effect = new EffectObject(TEXT(""));

		effects.push_back(effect);
	}

	_tstring fileName = TEXT(".png");
	
	if (!index)
	{
		fileName = TEXT("L") + fileName;
	}
	else
	{
		fileName = TEXT("R") + fileName;
	}

	if (isLongNote)
	{
		fileName = TEXT("long_") + fileName;
	}

	fileName = TEXT("image/effect_") + fileName;

	effect->LoadTexture(fileName);
	effect->SetPosition(xObject->position.x + xObject->centerPosition.x - effect->centerPosition.x,
						yObject->position.y + yObject->centerPosition.y - effect->size.y);
	effect->tick = GetTickCount();
	effect->bpm = bpm;
	effect->delay = delay;
	effect->color.a = 1.0f;
}
