#pragma once

#include <Object.h>

#include <Object/NoteObject.h>

using namespace BSEngine;

class NoteManagerObject : public Object
{
public:
	NoteManagerObject(_tstring _name);
	~NoteManagerObject();

	vector<NoteObject*> notes;

	float currentPositionRate;

	void Update() override;
	void Render() override;

	void LoadData(_tstring datas);

private:
	Object* note;
	Object* longNote;
};