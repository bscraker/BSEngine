#pragma once

#include <Object.h>

using namespace BSEngine;

class ComboObject : public Object
{
public:
	ComboObject(_tstring _name);
	~ComboObject();

	virtual void Render() override;

	UINT combo;
};