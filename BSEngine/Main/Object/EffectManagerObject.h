#pragma once

#include <Object.h>

#include <Object/EffectObject.h>

using namespace BSEngine;

class EffectManagerObject : public Object
{
public:
	EffectManagerObject(_tstring _name);
	~EffectManagerObject();

	void Update() override;
	void Render() override;

	void CreateEffect(Object* xObject, Object* yObject, bool index, bool isLongNote, float bpm, ULONG delay = 0);

private:
	vector<EffectObject*> effects;
};