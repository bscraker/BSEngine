#pragma once

#include <Object.h>

using namespace BSEngine;

class ScoreObject : public Object
{
public:
	ScoreObject(_tstring _name);
	~ScoreObject();

	virtual void Render() override;

	float score;
};