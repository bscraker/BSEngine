#pragma once

#include <Object.h>

using namespace BSEngine;

class EffectObject : public Object
{
public:
	EffectObject(_tstring _name);
	~EffectObject();

	virtual void Update() override;

	ULONG tick;
	float bpm;

	ULONG delay;
};