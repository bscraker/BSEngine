#include "JudgementObject.h"

JudgementObject::JudgementObject(_tstring _name) : Object(_name)
{
	tick = 0UL;
}

JudgementObject::~JudgementObject() {}

void JudgementObject::Update()
{
	if (!visible)
	{
		return;
	}

	float rate = (1.5f * (60000 / bpm) - (GetTickCount() - tick)) / (1.5f * 60000 / bpm);

	if (rate < 0.0f)
	{
		visible = false;

		return;
	}

	float sizeRate = 0.1f + rate;

	if (sizeRate < 1.0f)
	{
		sizeRate = 1.0f;
	}

	float alphaRate = 0.5f - rate;

	if (alphaRate < 0.0f)
	{
		alphaRate = 0.0f;
	}

	SetSize(texture.info.Width * sizeRate,
			texture.info.Height * sizeRate,
			true);

	color.a = (float)cos(alphaRate * M_PI);
}

void JudgementObject::Judge(UINT judge, float _bpm)
{
	if (judge == 2)
	{
		LoadTexture(TEXT("image/judgement_excellent.png"));
	}
	else if (judge == 1)
	{
		LoadTexture(TEXT("image/judgement_great.png"));
	}
	else if (judge == 0)
	{
		LoadTexture(TEXT("image/judgement_miss.png"));
	}

	SetPosition(0.0f, 0.0f);

	tick = GetTickCount();
	bpm = _bpm;

	visible = true;
}
