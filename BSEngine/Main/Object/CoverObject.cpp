#include "CoverObject.h"

CoverObject::CoverObject(_tstring _name) : Object(_name)
{
	tick = 0UL;
	maxRate = 1.0f;
	time = 0UL;
}

CoverObject::~CoverObject() {}

void CoverObject::Update()
{
	if (!visible)
	{
		return;
	}

	float rate = 1.0f - ((float)(GetTickCount() - (LONG)tick) / (LONG)time);

	if (rate <= 0.0f)
	{
		visible = false;

		return;
	}

	color.a = (float)sin(rate * M_PI / 2) * maxRate;
}

void CoverObject::Cover(bool isWhite, float _maxRate, ULONG _time)
{
	if (isWhite)
	{
		LoadTexture(TEXT("image/white.png"));
	}
	else
	{
		LoadTexture(TEXT("image/black.png"));
	}

	tick = GetTickCount();
	maxRate = _maxRate;
	time = _time;
	
	visible = true;
}
