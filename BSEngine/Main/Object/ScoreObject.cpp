#include "ScoreObject.h"

ScoreObject::ScoreObject(_tstring _name) : Object(_name)
{
	score = 0.0f;
}

ScoreObject::~ScoreObject() {}

void ScoreObject::Render()
{
	TCHAR frontScore[4];
	TCHAR backScore[2];

	_itot_s((int)score, frontScore, 4, 10);
	_itot_s((int)(score * 10) % 10, backScore, 2, 10);

	int count = lstrlen(frontScore);

	for (int i = 0; i < count; ++i)
	{
		LoadTexture(TEXT("image/score_") + to_tstring(frontScore[i] - TEXT('0')) + TEXT(".png"));
		SetPosition(734.0f - ((count - i) * size.x), 7.0f);
		Object::Render();
	}

	LoadTexture(TEXT("image/score_dot.png"));
	SetPosition(734.0f, 7.0f);
	Object::Render();

	LoadTexture(TEXT("image/score_") + to_tstring(backScore[0] - TEXT('0')) + TEXT(".png"));
	SetPosition(734.0f + size.x, 7.0f);
	Object::Render();

	LoadTexture(TEXT("image/score_percent.png"));
	SetPosition(734.0f + (2.0f * size.x), 7.0f);
	Object::Render();
}