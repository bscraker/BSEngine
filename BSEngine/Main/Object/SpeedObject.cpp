#include "SpeedObject.h"

SpeedObject::SpeedObject(_tstring _name) : Object(_name)
{
	order = 0;
	beatNumerator = 0;
	beatDenominator = 1;
	speed = 1.0f;

	time = 0UL;
}

SpeedObject::~SpeedObject() {}