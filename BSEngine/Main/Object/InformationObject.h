#pragma once

#include <Object.h>

using namespace BSEngine;

class InformationObject : public Object
{
public:
	InformationObject(_tstring _name);
	~InformationObject();

	virtual void Render() override {};

	void LoadData(_tstring data);

	_tstring musicName;
	_tstring artist;
	float startBPM;
	int startTime;
	int previewStartTime;
	int previewPlayTime;
	float musicVolume;
	float hitVolume;
	int hardDifficulty;
	int normalDifficulty;
	int easyDifficulty;
};