#include "SpeedManagerObject.h"

SpeedManagerObject::SpeedManagerObject(_tstring _name) : Object(_name) {}

SpeedManagerObject::~SpeedManagerObject() {}

void SpeedManagerObject::LoadData(_tstring datas)
{
	_tstringstream datasStream(datas);
	_tstring data;

	while (getline(datasStream, data, TEXT('\n')))
	{
		_tstringstream dataStream(data);
		_tstring token;

		SpeedObject* speed = new SpeedObject(TEXT(""));

		getline(dataStream, token, TEXT('|'));
		speed->order = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		speed->beatNumerator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		speed->beatDenominator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		speed->speed = (float)_ttof(token.c_str());

		speeds.push_back(speed);
	}
}

void SpeedManagerObject::ConvertPositionRate(vector<NoteObject*>* notes, float speed)
{
	for each (NoteObject* note in *notes)
	{
		float positionRate = 0;
		ULONG lastTime = 0;
		float lastSpeed = 1.0f;

		for each (SpeedObject* _speed in speeds)
		{
			if (_speed->time < note->time)
			{
				positionRate += (_speed->time - lastTime) * speed * lastSpeed / 2000;
				lastTime = _speed->time;
				lastSpeed = _speed->speed;
			}
			else
			{
				break;
			}
		}

		positionRate += (note->time - lastTime) * speed * lastSpeed / 2000;
		note->positionRate = positionRate;
	}
}

float SpeedManagerObject::ConvertPositionRate(LONG time, float speed)
{
	float positionRate = 0;
	ULONG lastTime = 0;
	float lastSpeed = 1.0f;

	if (time > 0)
	{
		for each (SpeedObject* _speed in speeds)
		{
			if ((LONG)_speed->time < time)
			{
				positionRate += (_speed->time - lastTime) * speed * lastSpeed / 2000;
				lastTime = _speed->time;
				lastSpeed = _speed->speed;
			}
			else
			{
				break;
			}
		}

		positionRate += (time - lastTime) * speed * lastSpeed / 2000;
	}
	else
	{
		positionRate += time * speed * lastSpeed / 2000;
	}

	return positionRate;
}
