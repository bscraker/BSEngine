#include "BPMObject.h"

BPMObject::BPMObject(_tstring _name) : Object(_name)
{
	order = 0;
	beatNumerator = 0;
	beatDenominator = 1;
	bpm = 0.0f;
}

BPMObject::~BPMObject() {}