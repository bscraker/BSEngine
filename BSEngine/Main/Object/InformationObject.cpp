#include "InformationObject.h"

InformationObject::InformationObject(_tstring _name) : Object(_name)
{
	name = TEXT("");
	artist = TEXT("");
	startBPM = 0.0f;
	startTime = 0;
	previewStartTime = 0;
	previewPlayTime = 0;
	musicVolume = 1.0f;
	hitVolume = 1.0f;
	hardDifficulty = 0;
	normalDifficulty = 0;
	easyDifficulty = 0;
}

InformationObject::~InformationObject() {}

void InformationObject::LoadData(_tstring data)
{
	_tstringstream dataStream(data);
	_tstring token;

	getline(dataStream, token, TEXT('|'));
	musicName = token;

	getline(dataStream, token, TEXT('|'));
	artist = token;

	getline(dataStream, token, TEXT('|'));
	startBPM = (float)_ttof(token.c_str());

	getline(dataStream, token, TEXT('|'));
	startTime = _ttoi(token.c_str());

	getline(dataStream, token, TEXT('|'));
	previewStartTime = _ttoi(token.c_str());

	getline(dataStream, token, TEXT('|'));
	previewPlayTime = _ttoi(token.c_str());

	getline(dataStream, token, TEXT('|'));
	musicVolume = (float)_ttof(token.c_str());

	getline(dataStream, token, TEXT('|'));
	hitVolume = (float)_ttof(token.c_str());

	getline(dataStream, token, TEXT('|'));
	hardDifficulty = _ttoi(token.c_str());

	getline(dataStream, token, TEXT('|'));
	normalDifficulty = _ttoi(token.c_str());

	getline(dataStream, token, TEXT('|'));
	easyDifficulty = _ttoi(token.c_str());
}
