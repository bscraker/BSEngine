#pragma once

#include <Object.h>

#include <Object/BPMObject.h>
#include <Object/NoteObject.h>
#include <Object/SpeedObject.h>

using namespace BSEngine;

class BPMManagerObject : public Object
{
public:
	BPMManagerObject(_tstring _name);
	~BPMManagerObject();

	void Render() override {};

	void LoadData(_tstring datas);

	void ConvertTime(vector<NoteObject*>* notes, float startBPM);
	void ConvertTime(vector<SpeedObject*>* speeds, float startBPM);

	float GetBPM(ULONG _time, float startBPM);

private:
	vector<BPMObject*> bpms;
};