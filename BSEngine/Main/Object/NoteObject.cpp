#include "NoteObject.h"

NoteObject::NoteObject(_tstring _name) : Object(_name)
{
	order = 0;
	beatNumerator = 0;
	beatDenominator = 1;
	positionNumerator = 0;
	positionDenominator = 1;
	isLongNote = false;

	time = 0;

	positionRate = 0.0f;

	isChecked = false;
	isMissing = false;
}

NoteObject::~NoteObject() {}