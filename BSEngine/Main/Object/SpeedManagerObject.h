#pragma once

#include <Object.h>

#include <Object/SpeedObject.h>
#include <Object/NoteObject.h>

using namespace BSEngine;

class SpeedManagerObject : public Object
{
public:
	SpeedManagerObject(_tstring _name);
	~SpeedManagerObject();

	vector<SpeedObject*> speeds;

	void Render() override {};

	void LoadData(_tstring datas);

	void ConvertPositionRate(vector<NoteObject*>* notes, float speed);
	float ConvertPositionRate(LONG time, float speed);
};