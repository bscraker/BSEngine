#include "NoteManagerObject.h"

NoteManagerObject::NoteManagerObject(_tstring _name) : Object(_name)
{
	currentPositionRate = 0.0f;

	note = new Object(TEXT("note"));
	longNote = new Object(TEXT("longNote"));
}

NoteManagerObject::~NoteManagerObject()
{
	delete note;
	delete longNote;
}

void NoteManagerObject::Update()
{
	for each (NoteObject* _note in notes)
	{
		float positionRate = _note->positionRate - currentPositionRate;

		if (positionRate >= -0.2f &&
			positionRate <= 1.0f)
		{
			float rate = ((1.0f - positionRate) * 3.0f / 8.0f) + (5.0f / 8.0f);

			_note->SetSize(_note->texture.info.Width * rate,
						  _note->texture.info.Height * rate);

			float x = ((800.0f * rate) - _note->size.x) *
				      _note->positionNumerator / _note->positionDenominator +
					  (800.0f * (1.0f - rate) / 2.0f);

			float y = 500.0f * (1.0f - positionRate) - _note->centerPosition.y;

			_note->SetPosition(x, y);
		}
		else if (positionRate > 1.0f)
		{
			break;
		}
	}
}

void NoteManagerObject::Render()
{
	int MAX = notes.size();

	for (int i = 0; i < MAX; ++i)
	{
		NoteObject* _note = notes[i];

		float positionRate = _note->positionRate - currentPositionRate;

		if (_note->isLongNote)
		{
			float nextPositionRate = notes[i + 1]->positionRate - currentPositionRate;

			if (positionRate <= 1.0f &&
				nextPositionRate >= -0.2f)
			{
				if (positionRate < -0.2f)
				{
					positionRate = -0.2f;
				}

				if (nextPositionRate > 1.0f)
				{
					nextPositionRate = 1.0f;
				}

				bool showNote = false;

				if (positionRate <= 0.0f &&
					!_note->isMissing &&
					!notes[i + 1]->isMissing)
				{
					if (nextPositionRate <= 0.0f)
					{
						continue;
					}

					if (name == TEXT("noteManager_L"))
					{
						note->LoadTexture(TEXT("image/note_L.png"));
						longNote->LoadTexture(TEXT("image/slide_L_light.png"));
					}
					else if (name == TEXT("noteManager_R"))
					{
						note->LoadTexture(TEXT("image/note_R.png"));
						longNote->LoadTexture(TEXT("image/slide_R_light.png"));
					}

					positionRate = 0.0f;

					showNote = true;
				}
				else
				{
					if (!_note->isMissing &&
						positionRate < 0.0f)
					{
						if (nextPositionRate > 0.0f)
						{
							showNote = true;
						}

						positionRate = 0.0f;
					}

					if (name == TEXT("noteManager_L"))
					{
						longNote->LoadTexture(TEXT("image/slide_L_normal.png"));
					}
					else if (name == TEXT("noteManager_R"))
					{
						longNote->LoadTexture(TEXT("image/slide_R_normal.png"));
					}
				}

				int startPosition = (int)(500.0f * (1.0f - positionRate));
				int endPosition = (int)(500.0f * (1.0f - nextPositionRate));

				while (startPosition >= endPosition)
				{
					float rate = startPosition / 500.0f;

					longNote->SetSize(longNote->texture.info.Width * ((rate * 3.0f / 8.0f) + (5.0f / 8.0f)),
									  (float)longNote->texture.info.Height);

					float x = ((300.0f * rate + 500.0f) - longNote->size.x) *
							  _note->positionNumerator / _note->positionDenominator +
							  (300.0f * (1.0f - rate) / 2.0f);

					longNote->SetPosition(x, (float)startPosition);

					longNote->Render();

					--startPosition;
				}

				if (showNote)
				{
					note->SetPosition((800.0f - note->size.x) *
						_note->positionNumerator / _note->positionDenominator,
						500.0f - note->centerPosition.y);

					note->Render();
				}
			}
		}
		else if (positionRate > 1.0f)
		{
			break;
		}
	}

	for (int i = notes.size() - 1; i >= 0; --i)
	{
		NoteObject* _note = notes[i];

		float positionRate = _note->positionRate - currentPositionRate;

		if (positionRate >= -0.2f &&
			positionRate <= 1.0f)
		{
			if (_note->isLongNote)
			{
				if (positionRate > 0.0f ||
					_note->isMissing)
				{
					_note->Render();
				}
			}
			else
			{
				_note->Render();
			}
		}
		else if (positionRate < -0.2f)
		{
			break;
		}
	}
}

void NoteManagerObject::LoadData(_tstring datas)
{
	_tstringstream datasStream(datas);
	_tstring data;

	while (getline(datasStream, data, TEXT('\n')))
	{
		_tstringstream dataStream(data);
		_tstring token;

		NoteObject* _note = new NoteObject(TEXT(""));

		if (name == TEXT("noteManager_L"))
		{
			_note->LoadTexture(TEXT("image/note_L.png"));
		}
		else if (name == TEXT("noteManager_R"))
		{
			_note->LoadTexture(TEXT("image/note_R.png"));
		}

		getline(dataStream, token, TEXT('|'));
		_note->order = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_note->beatNumerator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_note->beatDenominator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_note->positionNumerator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_note->positionDenominator = _ttoi(token.c_str());

		getline(dataStream, token, TEXT('|'));
		_note->isLongNote = (_ttoi(token.c_str()) == 1);

		notes.push_back(_note);
	}
}
