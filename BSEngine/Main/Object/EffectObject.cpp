#include "EffectObject.h"

EffectObject::EffectObject(_tstring _name) : Object(_name)
{
	tick = 0UL;
	bpm = 0.0f;

	delay = 0UL;
}

EffectObject::~EffectObject() {}

void EffectObject::Update()
{
	LONG time = GetTickCount() - (LONG)tick - (LONG)delay;

	if (time > 0)
	{
		float rate = (1.5f * (60000 / bpm) - time) / (1.5f * 60000 / bpm);

		if (rate <= 0.0f)
		{
			visible = false;

			return;
		}

		color.a = (float)sin(rate * M_PI / 2);
	}
}
