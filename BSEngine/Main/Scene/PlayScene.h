#pragma once

#include <BSEngine.h>
#include <Scene.h>
#include <SerialManager.h>
#include <FileManager.h>
#include <SoundManager.h>

#include <Object/EffectManagerObject.h>
#include <Object/NoteManagerObject.h>
#include <Object/SpeedManagerObject.h>
#include <Object/BPMManagerObject.h>
#include <Object/JudgementObject.h>
#include <Object/InformationObject.h>
#include <Object/ComboObject.h>
#include <Object/ScoreObject.h>
#include <Object/CoverObject.h>

using namespace BSEngine;

class PlayScene : public Scene
{
public:
	PlayScene(_tstring _name,
			  _tstring _portName,
			  _tstring _musicName,
			  UINT   _difficulty,
			  float  _speed);

	~PlayScene();

	virtual void Initialize() override;
	virtual void Update() override;

private:
	enum DATATYPE
	{
		KNOB_L,
		KNOB_R,
		JOYSTICK_X,
		JOYSTICK_Y,
		CANCEL,
		JOYSTICK_BUTTON,
		LENGTH
	};

	_tstring portName;
	Serial serial;

	_tstring datas;
	UINT data[DATATYPE::LENGTH];
	bool isCancelled;

	Object* background;
	EffectManagerObject* effectManager;
	Object* knob[2];
	InformationObject* information;
	NoteManagerObject* noteManagers[2];
	SpeedManagerObject* speedManager;
	BPMManagerObject* bpmManager;
	Object* album;
	Object* albumImage;
	JudgementObject* judgement;
	ComboObject* combo;
	ScoreObject* score;
	Object* rank;
	CoverObject* cover;

#ifdef _DEBUG
	float autoX[2];
	float autoXRate[2];
#endif

	_tstring musicName;
	UINT difficulty;
	float speed;

	UINT excellent;
	UINT great;
	UINT miss;

	bool fullCombo;

	Sound music;
	Sound hit;

	LONG delayTick;
	bool isPlayed;
	bool isFinished;

	D3DXCOLOR ledColor[2];
	ULONG ledTick[2];
	float ledBPM[2];

	void PreLoadTexture();

	void UpdateData();
	_tstring ParseData(_tstring* data);

	void UpdateKnob();
	void UpdateNote();
	void UpdateLED();

	void Finish();
};