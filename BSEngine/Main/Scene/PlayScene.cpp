#include "PlayScene.h"

PlayScene::PlayScene(_tstring _name,
	_tstring _portName,
	_tstring _musicName,
	UINT   _difficulty,
	float  _speed) : Scene(_name)
{
	portName = _portName;

	datas = TEXT("");
	ZeroMemory(data, DATATYPE::LENGTH);
	data[0] = 0;
	data[1] = 1023;
	isCancelled = false;

	background = NULL;
	knob[0] = NULL;
	knob[1] = NULL;
	effectManager = NULL;
	information = NULL;
	noteManagers[0] = NULL;
	noteManagers[1] = NULL;
	speedManager = NULL;
	bpmManager = NULL;
	album = NULL;
	albumImage = NULL;
	judgement = NULL;
	combo = NULL;
	score = NULL;
	rank = NULL;
	cover = NULL;

#ifdef _DEBUG
	autoX[0] = 0.0f;
	autoX[1] = 1.0f;
	autoXRate[0] = -1.0f;
	autoXRate[1] = -1.0f;
#endif

	musicName = _musicName;
	difficulty = _difficulty;
	speed = _speed;

	miss = 0;
	great = 0;
	excellent = 0;

	fullCombo = false;

	delayTick = GetTickCount();
	isPlayed = false;
	isFinished = false;

	ledColor[0] = {0.0f, 0.0f, 0.0f, 0.0f};
	ledColor[1] = {0.0f, 0.0f, 0.0f, 0.0f};
	ledTick[0] = 0UL;
	ledTick[1] = 0UL;
	ledBPM[0] = 0.0f;
	ledBPM[1] = 0.0f;
}

PlayScene::~PlayScene() {}

void PlayScene::Initialize()
{
	PreLoadTexture();

	music = SoundManager::GetInstance()->LoadSound(TEXT("music/") + musicName + TEXT("/music.mp3"));
	hit = SoundManager::GetInstance()->LoadSound(TEXT("music/") + musicName + TEXT("/hit.wav"));

	serial = SerialManager::GetInstance()->OpenSerial(portName);

	background = new Object(TEXT("background"));
	background->LoadTexture(TEXT("image/background.png"));
	objectManager.AddObject(background);

	effectManager = new EffectManagerObject(TEXT("effectManager"));
	objectManager.AddObject(effectManager);

	knob[0] = new Object(TEXT("knob_L"));
	knob[0]->LoadTexture(TEXT("image/knob_L.png"));
	objectManager.AddObject(knob[0]);

	knob[1] = new Object(TEXT("knob_R"));
	knob[1]->LoadTexture(TEXT("image/knob_R.png"));
	objectManager.AddObject(knob[1]);

	_tstring path = TEXT("music/") + musicName + TEXT("/");
	_tstring difficultyName = TEXT("");

	if (difficulty == 0)
	{
		difficultyName = TEXT("easy");
	}
	else if (difficulty == 1)
	{
		difficultyName = TEXT("normal");
	}
	else
	{
		difficultyName = TEXT("hard");
	}

	information = new InformationObject(TEXT("information"));
	information->LoadData(FileManager::GetInstance()->ReadText(path + TEXT("information.dat")));
	objectManager.AddObject(information);

	music.SetVolume(information->musicVolume);
	hit.SetVolume(information->hitVolume);

	noteManagers[0] = new NoteManagerObject(TEXT("noteManager_L"));
	noteManagers[0]->LoadData(FileManager::GetInstance()->ReadText(path + TEXT("note_") + difficultyName + TEXT("_l.dat")));
	objectManager.AddObject(noteManagers[0]);

	noteManagers[1] = new NoteManagerObject(TEXT("noteManager_R"));
	noteManagers[1]->LoadData(FileManager::GetInstance()->ReadText(path + TEXT("note_") + difficultyName + TEXT("_r.dat")));
	objectManager.AddObject(noteManagers[1]);

	speedManager = new SpeedManagerObject(TEXT("speedManager"));
	speedManager->LoadData(FileManager::GetInstance()->ReadText(path + TEXT("note_") + difficultyName + TEXT("_speed.dat")));
	objectManager.AddObject(speedManager);

	bpmManager = new BPMManagerObject(TEXT("bpmManager"));
	bpmManager->LoadData(FileManager::GetInstance()->ReadText(path + TEXT("note_") + difficultyName + TEXT("_bpm.dat")));
	objectManager.AddObject(bpmManager);

	bpmManager->ConvertTime(&speedManager->speeds, information->startBPM);
	bpmManager->ConvertTime(&noteManagers[0]->notes, information->startBPM);
	bpmManager->ConvertTime(&noteManagers[1]->notes, information->startBPM);

	speedManager->ConvertPositionRate(&noteManagers[0]->notes, speed);
	speedManager->ConvertPositionRate(&noteManagers[1]->notes, speed);

	album = new Object(TEXT("album"));
	album->LoadTexture(TEXT("image/album.png"));
	album->SetPosition(2.0f, 1.0f);
	objectManager.AddObject(album);

	albumImage = new Object(TEXT("album"));
	albumImage->LoadTexture(path + TEXT("image.png"));
	albumImage->SetSize(88.0f, 88.0f);
	albumImage->SetPosition(album->position + album->centerPosition - albumImage->centerPosition);
	objectManager.AddObject(albumImage);

	judgement = new JudgementObject(TEXT("judgement"));
	judgement->visible = false;
	objectManager.AddObject(judgement);

	combo = new ComboObject(TEXT("combo"));
	combo->visible = false;
	objectManager.AddObject(combo);

	score = new ScoreObject(TEXT("score"));
	objectManager.AddObject(score);

	rank = new Object(TEXT("rank"));
	rank->SetPosition(720.0f, 42.0f);
	rank->visible = false;
	objectManager.AddObject(rank);

	cover = new CoverObject(TEXT("cover"));
	cover->Cover(false, 1.0f, 1000UL);
	objectManager.AddObject(cover);
}

void PlayScene::Update()
{
	UpdateData();
	UpdateKnob();
	UpdateNote();
	UpdateLED();

	if (data[DATATYPE::CANCEL])
	{
		if (!isCancelled)
		{
			PostQuitMessage(0);

			isCancelled = true;
		}
	}
	else
	{
		isCancelled = false;
	}
	
	if (isPlayed && isFinished &&
		(LONG)GetTickCount() > delayTick)
	{
		PostQuitMessage(0);
	}

	Scene::Update();
}

void PlayScene::PreLoadTexture()
{
	vector<_tstring> fileNames = FileManager::GetInstance()->GetFileNames(TEXT("image"));

	for each (_tstring fileName in fileNames)
	{
		if (fileName.rfind(TEXT(".png")) != -1)
		{
			TextureManager::GetInstance()->LoadTexture(TEXT("image/") + fileName);
		}
	}

	TextureManager::GetInstance()->LoadTexture(TEXT("music/") + musicName + TEXT("/image.png"));
}

void PlayScene::UpdateData()
{
	datas += serial.ReadString();

	int lastIndex2 = datas.find_last_of('|');

	if (lastIndex2 != -1)
	{
		int lastIndex = datas.find_last_of('!', lastIndex2 - 1);

		if (lastIndex != -1 && lastIndex < lastIndex2)
		{
			_tstring currentData = datas.substr(lastIndex + 1, lastIndex2 - (lastIndex + 1));

			datas = datas.substr(lastIndex2 + 1);

			for (int i = 0; i < DATATYPE::LENGTH; ++i)
			{
				data[i] = _ttoi(ParseData(&currentData).c_str());
			}
		}
	}
}

_tstring PlayScene::ParseData(_tstring* data)
{
	int index = data->find(TEXT(" "));

	_tstring _data = TEXT("");

	if (index > 0)
	{
		_data = data->substr(0, index);
		*data = data->substr(index + 1);
	}

	return _data;
}

void PlayScene::UpdateKnob()
{
	for (int i = 0; i < 2; ++i)
	{
		knob[i]->SetPosition((800.0f - knob[i]->size.x) * data[DATATYPE::KNOB_L + i] / 1023.0f,
							 500.0f - knob[i]->centerPosition.y + 5.0f);
	}
}

void PlayScene::UpdateNote()
{
	float currentPositionRate;
	
	if (music.IsPlaying())
	{
		currentPositionRate = speedManager->ConvertPositionRate((LONG)music.GetPosition() - information->startTime, speed);
	}
	else if (isPlayed)
	{
		currentPositionRate = speedManager->ConvertPositionRate((LONG)music.GetLength() - information->startTime, speed);
	}
	else
	{
		LONG delay = 5000 + delayTick - GetTickCount();

		currentPositionRate = speedManager->ConvertPositionRate(-(information->startTime + delay), speed);

		if (delay <= 0)
		{
			music.Play();
			isPlayed = true;
		}
	}

	noteManagers[0]->currentPositionRate = currentPositionRate;
	noteManagers[1]->currentPositionRate = currentPositionRate;

	if (music.IsPlaying())
	{
		LONG musicPosition = (LONG)music.GetPosition() - information->startTime;

		for each (NoteManagerObject* noteManager in noteManagers)
		{
			int MAX = noteManager->notes.size();

#ifdef _DEBUG
			bool autoMoved[2] = {false, false};
#endif

			for (int i = 0; i < MAX; ++i)
			{
				NoteObject* note = noteManager->notes[i];

				if (!note->isChecked)
				{
					DATATYPE _dataType;

					if (noteManager->name == TEXT("noteManager_L"))
					{
						_dataType = KNOB_L;
					}
					else if (noteManager->name == TEXT("noteManager_R"))
					{
						_dataType = KNOB_R;
					}

					float rate = 800.0f *
						(((LONG)data[_dataType] / 1023.0f) -
						((float)note->positionNumerator / note->positionDenominator)) /
						note->size.x;

					rate = abs(rate);

#ifdef _DEBUG
					float notePositionRate = 1.0f - (note->positionRate - currentPositionRate);

					if (!autoMoved[_dataType])
					{
						if (notePositionRate <= 1.0f && notePositionRate >= 0.0f)
						{
							if (autoXRate[_dataType] < 0.0f)
							{
								autoXRate[_dataType] = notePositionRate;
							}

							float positionRate =
								autoX[_dataType] +
								(((float)note->positionNumerator / note->positionDenominator) - autoX[_dataType]) * ((notePositionRate - autoXRate[_dataType]) / (1.0f - autoXRate[_dataType]));

							data[_dataType] = (UINT)(1023 * positionRate);
						}

						autoMoved[_dataType] = true;
					}
#endif

					if (i && noteManager->notes[i - 1]->isLongNote && !note->isMissing)
					{
						if (rate >= 0.4f)
						{
							note->isMissing = true;
						}
					}

					if ((LONG)note->time <= musicPosition)
					{
						float currentBPM = bpmManager->GetBPM(musicPosition, information->startBPM);

						if (i && noteManager->notes[i - 1]->isLongNote && noteManager->notes[i - 1]->isMissing)
						{
							note->isMissing = true;
						}

						if (rate < 0.2f && !note->isMissing)
						{
							judgement->Judge(2, currentBPM);

							++excellent;
							++combo->combo;
						}
						else if (rate < 0.4f && !note->isMissing)
						{
							judgement->Judge(1, currentBPM);

							++great;
							++combo->combo;
						}
						else
						{
							judgement->Judge(0, currentBPM);

							++miss;
							combo->combo = 0;

							note->isMissing = true;
						}

						if (!note->isMissing)
						{
							if (i == MAX - 1 &&
								miss == 0)
							{
								if (fullCombo)
								{
									cover->Cover(true, 0.5f, 400UL);
								}
								else
								{
									fullCombo = true;
								}
							}

							int index = _dataType - DATATYPE::KNOB_L;
							ULONG delay = 0UL;

							if (note->isLongNote)
							{
								delay = noteManager->notes[i + 1]->time - note->time;
							}

							effectManager->CreateEffect(note, knob[index], (index == 1), note->isLongNote, currentBPM, delay);

							note->visible = false;

							hit.Play();

							ledTick[index] = music.GetPosition();
							ledBPM[index] = currentBPM;
						}

						combo->visible = true;

						score->score = 100.0f * 
									   ((excellent * 5) + (great * 2)) /
									   ((excellent + great + miss) * 5);

						_tstring _rank = TEXT("");

						if (score->score >= 95.0f)
						{
							_rank = TEXT("ss");
						}
						else if (score->score >= 90.0f)
						{
							_rank = TEXT("s");
						}
						else if (score->score >= 80.0f)
						{
							_rank = TEXT("a");
						}
						else if (score->score >= 70.0f)
						{
							_rank = TEXT("b");
						}
						else
						{
							_rank = TEXT("c");
						}

						rank->LoadTexture(TEXT("image/rank_") + _rank + TEXT(".png"));
						rank->visible = true;

						note->isChecked = true;

#ifdef _DEBUG
						autoX[_dataType] = (float)note->positionNumerator / note->positionDenominator;
						autoXRate[_dataType] = -1.0f;
						autoMoved[_dataType] = false;
#endif
					}
					else
					{
						break;
					}
				}
			}
		}
	}

	if (isPlayed && !isFinished &&
		music.GetPosition() == 0 && !music.IsPlaying())
	{
		Finish();

		isFinished = true;
	}
}

void PlayScene::UpdateLED()
{
	for (int i = 0; i < 2; ++i)
	{
		if (ledTick[i])
		{
			LONG time = music.GetPosition() - (LONG)ledTick[i];

			float rate = ((60000 / ledBPM[i]) - time) / (60000 / ledBPM[i]);

			if (rate < 0.0f)
			{
				rate = 0.0f;
			}

			ledColor[i].r = rate * i;
			ledColor[i].g = rate * ((1 - i) / 4.0f);
			ledColor[i].b = rate * (1.0f - (i * 3.0f / 4.0f));
		}
	}

	if (music.IsPlaying())
	{
		serial.WriteString(TEXT("!") +
						   to_tstring((int)(ledColor[0].r * 255.0f)) + TEXT(" ") +
						   to_tstring((int)(ledColor[0].g * 255.0f)) + TEXT(" ") +
						   to_tstring((int)(ledColor[0].b * 255.0f)) + TEXT(" ") +
						   to_tstring((int)(ledColor[1].r * 255.0f)) + TEXT(" ") +
						   to_tstring((int)(ledColor[1].g * 255.0f)) + TEXT(" ") +
						   to_tstring((int)(ledColor[1].b * 255.0f)) + TEXT(" ") +
						   TEXT("|"));
	}
}

void PlayScene::Finish()
{
	if (miss == 0)
	{
		if (great == 0)
		{
			// ����Ʈ
		}
		else
		{
			// Ǯ��
		}
	}

	_tstring difficultyName = TEXT("");

	if (difficulty == 0)
	{
		difficultyName = TEXT("easy");
	}
	else if (difficulty == 1)
	{
		difficultyName = TEXT("normal");
	}
	else
	{
		difficultyName = TEXT("hard");
	}

	float oldScore = (float)_ttof(FileManager::GetInstance()->ReadText(TEXT("music/") + musicName + TEXT("/score_") + difficultyName + TEXT(".dat")).c_str());

	if (score->score > oldScore)
	{
		FileManager::GetInstance()->WriteText(TEXT("music/") + musicName + TEXT("/score_") + difficultyName + TEXT(".dat"), to_tstring(score->score));
	}

	delayTick = GetTickCount() + 5000;
}
