#include "ObjectManager.h"

namespace BSEngine
{
	ObjectManager::ObjectManager()
	{
		camera_x = 0;
		camera_y = 0;
	}


	ObjectManager::~ObjectManager()	{}

	void ObjectManager::AddObject(Object* object)
	{
		objects.push_back(object);
	}

	Object* ObjectManager::GetObject(_tstring name)
	{
		for each (Object* object in objects)
		{
			if (object->name == name)
			{
				return object;
			}
		}

		return NULL;
	}

	void ObjectManager::MoveCamera(int x, int y)
	{
		camera_x = x;
		camera_y = y;
	}

	void ObjectManager::Update()
	{
		for each (Object* object in objects)
		{
			object->Update();
		}
	}

	void ObjectManager::Render()
	{
		for each (Object* object in objects)
		{
			if (object->visible)
			{
				object->Render();
			}
		}
	}

	void ObjectManager::Release()
	{
		for each (Object* object in objects)
		{
			delete object;
		}

		objects.clear();
	}
}