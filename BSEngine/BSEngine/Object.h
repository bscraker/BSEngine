#pragma once

#include "BSEngine.h"
#include "TextureManager.h"

namespace BSEngine
{
	class Object
	{
	public:
		Object(_tstring _name);
		virtual ~Object();

		Texture			texture;
		vector<Texture>	textureAnimation;
		D3DXVECTOR2		size;
		D3DXVECTOR2		position;
		D3DXVECTOR2		centerPosition;
		D3DXCOLOR		color;

		_tstring		name;
		float			angle;
		bool			isUI;
		bool			visible;

		int				animation_now;
		int				animation_time;

		void LoadTexture(_tstring path, bool setSize = true, bool setCenterPosition = true);
		void LoadTextureAnimation(_tstring path, int startIndex, int endIndex);

		void SetSize(float x, float y, bool center = false);
		void SetSize(D3DXVECTOR2 size, bool center = false);
		void SetPosition(float x, float y);
		void SetPosition(D3DXVECTOR2 _position);
		void SetCenterPosition(float x, float y);
		void SetCenterPosition(D3DXVECTOR2 _position);
		
		bool Animate(_tstring path, int frame, float delay);

		virtual void Update() {};
		virtual void Render();
	};
}