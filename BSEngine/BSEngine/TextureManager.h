#pragma once

#include "BSEngine.h"
#include "Singleton.h"

namespace BSEngine
{
	struct Texture
	{
		_tstring			path;
		D3DXIMAGE_INFO		info;
		LPDIRECT3DTEXTURE9	texture;
	};

	class TextureManager : public CSingleton<TextureManager>
	{
	public:
		TextureManager();
		~TextureManager();

		vector<Texture> textures;

		Texture LoadTexture(_tstring path);
		void Release();

	private:
		Texture CreateTexture(_tstring path);
	};
}