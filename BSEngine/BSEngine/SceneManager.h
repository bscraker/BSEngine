#pragma once

#include "Singleton.h"
#include "Scene.h"

namespace BSEngine
{
	class SceneManager : public CSingleton<SceneManager>
	{
	public:
		SceneManager();
		~SceneManager();

		vector<Scene*>	scenes;
		Scene*			currentScene;

		void AddScene(Scene* _scene);
		void ChangeScene(_tstring _name);
		Scene* GetScene(_tstring _name);
		void DeleteScene(_tstring _name);

		void Update();
		void Render();

		void Release();
	};
}