#include "Engine.h"
#include "GraphicManager.h"
#include "SoundManager.h"
#include "SceneManager.h"
#include "SerialManager.h"

namespace BSEngine
{
	Engine::Engine() {}

	Engine::~Engine() {}

	void Engine::Initialize(Scene* scene, UINT width, UINT height, _tstring className, _tstring windowName, HICON icon /*= NULL*/, HCURSOR cursor /*= NULL*/, bool showCursor /*= true*/)
	{
		GraphicManager::GetInstance()->Initialize(
			width, height, className, windowName, icon, cursor, showCursor
			);

		SoundManager::GetInstance()->Initialize();

		SceneManager::GetInstance()->AddScene(scene);
		SceneManager::GetInstance()->ChangeScene(scene->name);
	}

	void Engine::Release()
	{
		SerialManager::GetInstance()->Release();

		SceneManager::GetInstance()->Release();

		SoundManager::GetInstance()->Release();

		TextureManager::GetInstance()->Release();

		GraphicManager::GetInstance()->Release();
	}

	void Engine::MainLoop()
	{
		MSG message;
		ZeroMemory(&message, sizeof(message));

		while (message.message != WM_QUIT)
		{
			if (PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&message);
				DispatchMessage(&message);
			}
			else
			{
				SceneManager::GetInstance()->Update();
				SoundManager::GetInstance()->system->update();

				GraphicManager::GetInstance()->RenderStart();
				SceneManager::GetInstance()->Render();
				GraphicManager::GetInstance()->RenderEnd();
			}
		}
	}
}