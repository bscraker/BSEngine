#include "TextureManager.h"
#include "GraphicManager.h"

namespace BSEngine
{
	TextureManager::TextureManager() {}

	TextureManager::~TextureManager() {}

	Texture TextureManager::LoadTexture(_tstring path)
	{
		for each (Texture texture in textures)
		{
			if (path == texture.path)
			{
				return texture;
			}
		}

		return CreateTexture(path);
	}

	void TextureManager::Release()
	{
		for each (Texture texture in textures)
		{
			texture.texture->Release();
		}

		textures.clear();
	}

	Texture TextureManager::CreateTexture(_tstring path)
	{
		Texture texture;
		texture.path = path;

		D3DXCreateTextureFromFileEx(GraphicManager::GetInstance()->device,
			path.c_str(),
			D3DX_DEFAULT_NONPOW2,
			D3DX_DEFAULT_NONPOW2,
			D3DX_DEFAULT,
			NULL,
			D3DFMT_UNKNOWN,
			D3DPOOL_MANAGED,
			D3DX_FILTER_LINEAR,
			D3DX_FILTER_NONE,
			NULL,
			&texture.info,
			NULL,
			&texture.texture);

		textures.push_back(texture);

		return texture;
	}
}