#pragma once

#include "Utility/tstring.h"

using namespace std;

namespace BSEngine
{
	class Scene;

	class Engine
	{
	public:
		Engine();
		~Engine();

		void Initialize(Scene* scene, UINT width, UINT height, _tstring className, _tstring windowName, HICON icon = NULL, HCURSOR cursor = NULL, bool showCursor = true);
		void Release();

		void MainLoop();
	};
}