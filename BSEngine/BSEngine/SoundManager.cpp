#include "SoundManager.h"

namespace BSEngine
{
	void Sound::Play()
	{
		if (IsPaused())
		{
			channel->setPaused(false);
		}
		else
		{
			SoundManager::GetInstance()->
				system->playSound(sound, NULL, false, &channel);
		}
	}

	bool Sound::IsPlaying()
	{
		bool isPlaying = false;

		channel->isPlaying(&isPlaying);

		return isPlaying;
	}

	void Sound::Pause()
	{
		if (IsPlaying())
		{
			channel->setPaused(true);
		}
	}

	bool Sound::IsPaused()
	{
		bool isPaused = false;

		channel->getPaused(&isPaused);

		return isPaused;
	}

	void Sound::Stop()
	{
		if (IsPlaying())
		{
			channel->stop();
		}
	}

	void Sound::SetPosition(UINT position)
	{
		channel->setPosition(position, FMOD_TIMEUNIT_MS);
	}

	UINT Sound::GetLength()
	{
		UINT length = 0;

		sound->getLength(&length, FMOD_TIMEUNIT_MS);

		return length;
	}

	UINT Sound::GetPosition()
	{
		UINT position = 0;

		channel->getPosition(&position, FMOD_TIMEUNIT_MS);

		return position;
	}

	void Sound::SetVolume(float volume)
	{
		channel->setVolume(volume);
	}

	float Sound::GetVolume()
	{
		float volume = 0.0f;

		channel->getVolume(&volume);

		return volume;
	}

	SoundManager::SoundManager()
	{
		system = NULL;
	}

	SoundManager::~SoundManager() {}

	void SoundManager::Initialize()
	{
		FMOD::System_Create(&system);
		system->init(32, FMOD_INIT_NORMAL, NULL);
	}

	void SoundManager::Release()
	{
		for each (Sound sound in sounds)
		{
			sound.sound->release();
		}

		if (system)
		{
			system->close();
			system->release();
		}
	}

	Sound SoundManager::LoadSound(_tstring path)
	{
		for each (Sound sound in sounds)
		{
			if (path == sound.path)
			{
				return sound;
			}
		}

		return CreateSound(path);
	}

	Sound SoundManager::CreateSound(_tstring path)
	{
		Sound sound;
		sound.path = path;

		system->createSound(
#ifdef _UNICODE
			Utility::ConvertString(path).c_str(),
#else
			path.c_str(),
#endif

			FMOD_DEFAULT, NULL, &sound.sound);
		system->playSound(sound.sound, NULL, true, &sound.channel);

		sound.channel->stop();

		sounds.push_back(sound);

		return sound;
	}
}