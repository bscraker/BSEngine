#pragma once

#include "Object.h"
#include <vector>

using namespace std;

namespace BSEngine
{
	class ObjectManager
	{
	public:
		ObjectManager();
		~ObjectManager();

		vector<Object*>	objects;
		int				camera_x;
		int				camera_y;

		void AddObject(Object* object);
		Object* GetObject(_tstring name);

		void MoveCamera(int x, int y);

		void Update();
		void Render();

		void Release();
	};
}