#include "Object.h"
#include "GraphicManager.h"

namespace BSEngine
{
	Object::Object(_tstring _name)
	{
		color = D3DCOLOR_ARGB(255, 255, 255, 255);

		name = _name;
		angle = 0;
		isUI = false;
		visible = true;

		animation_now = 0;
		animation_time = 0;
	}

	Object::~Object() {}

	void Object::LoadTexture(_tstring path, bool setSize, bool setCenterPosition)
	{
		texture = TextureManager::GetInstance()->LoadTexture(path);

		if (setSize)
		{
			SetSize((float)texture.info.Width, (float)texture.info.Height);
		}

		if (setCenterPosition)
		{
			SetCenterPosition((float)texture.info.Width / 2, (float)texture.info.Height / 2);
		}
	}

	void Object::LoadTextureAnimation(_tstring path, int startIndex, int endIndex)
	{
		int rate = 1;

		if (startIndex > endIndex)
		{
			rate = -1;
		}

		for (int i = startIndex; i != endIndex; i += rate)
		{
			textureAnimation.push_back(TextureManager::GetInstance()->LoadTexture(path + to_tstring(i)));
		}

		if (textureAnimation.size())
		{
			texture = textureAnimation[0];
		}
	}

	void Object::SetSize(float x, float y, bool center)
	{
		if (center)
		{
			SetPosition(
				position.x + centerPosition.x,
				position.y + centerPosition.y);
			SetSize(x, y);
			SetPosition(
				position.x - centerPosition.x,
				position.y - centerPosition.y);
		}
		else
		{
			SetCenterPosition(
				centerPosition.x * x / size.x,
				centerPosition.y * y / size.y);

			size.x = x;
			size.y = y;
		}
	}

	void Object::SetSize(D3DXVECTOR2 size, bool center)
	{
		SetSize(size.x, size.y, center);
	}

	void Object::SetPosition(float x, float y)
	{
		position.x = x;
		position.y = y;
	}

	void Object::SetPosition(D3DXVECTOR2 _position)
	{
		position = _position;
	}

	void Object::SetCenterPosition(float x, float y)
	{
		centerPosition.x = x;
		centerPosition.y = y;
	}

	void Object::SetCenterPosition(D3DXVECTOR2 _position)
	{
		centerPosition = _position;
	}

	bool Object::Animate(_tstring path, int Frame, float Delay)
	{
// 		if (GetTickCount() - animation_time >= Delay)
// 		{
// 			animation_time = GetTickCount();
// 
// 			if (animation_time == Frame)
// 			{
// 				animation_now = 0;
// 				animation_time = 0;
// 
// 				return true;
// 			}
// 
// 			animation_now++;
// 			path += to_string(animation_now);
// 			path += ".png";
// 
// 			LoadTexture(path);
// 		}

		return false;
	}

	void Object::Render()
	{
		GraphicManager::GetInstance()->RenderObject(this);
	}
}