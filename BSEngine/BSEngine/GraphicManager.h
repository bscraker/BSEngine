#pragma once

#include "BSEngine.h"
#include "Singleton.h"
#include "Object.h"

namespace BSEngine
{
	class GraphicManager : public CSingleton<GraphicManager>
	{
	public:
		GraphicManager();
		~GraphicManager();

		LPDIRECT3DDEVICE9   device;
		LPDIRECT3D9         d3d;
		LPD3DXSPRITE		sprite;
		LPD3DXFONT			font;
		WNDCLASSEX			wc;
		HWND				hWnd;

		void Initialize(UINT width, UINT height, _tstring className, _tstring windowName, HICON icon = NULL, HCURSOR cursor = NULL, bool showCursor = true);
		void Release();
		
		void CreateSprite();
		void CreateFont();

		void RenderObject(Object* object);
		void RenderText(_tstring text, int x, int y, float scale, float _angle, D3DXCOLOR color);

		void RenderStart();
		void RenderEnd();		
	};
}