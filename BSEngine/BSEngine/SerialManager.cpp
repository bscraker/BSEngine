#include "SerialManager.h"

namespace BSEngine
{
	_tstring Serial::ReadString()
	{
		char buffer;
		ULONG bufferCount;

		_tstring data = TEXT("");

		do
		{
			serial->Read(&buffer, 1, &bufferCount);

			if (bufferCount)
			{
				data += buffer;
			}
		}
		while (bufferCount == sizeof(buffer));

		return data;
	}

	void Serial::WriteString(_tstring data)
	{
		serial->Write(
#ifdef _UNICODE
			Utility::ConvertString(data).c_str()
#else
			data.c_str()
#endif
			);
	}

	SerialManager::SerialManager() {}

	SerialManager::~SerialManager()	{}

	Serial SerialManager::OpenSerial(_tstring portName)
	{
		for each (Serial serial in serials)
		{
			if (portName == serial.portName)
			{
				if (serial.serial->IsOpen())
				{
					serial.serial->Close();
					serial.serial->Open(portName.c_str());
					serial.serial->Setup();
				}

				return serial;
			}
		}

		return CreateSerial(portName);
	}

	void SerialManager::Release()
	{
		for each (Serial serial in serials)
		{
			serial.serial->Close();
			delete serial.serial;
		}

		serials.clear();
	}

	Serial SerialManager::CreateSerial(_tstring portName)
	{
		Serial serial;
		serial.portName = portName;
		serial.serial = new CSerial;

		serial.serial->Close();
		serial.serial->Open(portName.c_str());
		serial.serial->Setup();

		serials.push_back(serial);

		return serial;
	}
}