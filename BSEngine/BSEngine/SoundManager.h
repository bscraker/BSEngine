#pragma once

#include "BSEngine.h"
#include "Singleton.h"

using namespace std;

namespace BSEngine
{
	struct Sound
	{
		_tstring			path;
		FMOD::Sound*	sound;
		FMOD::Channel*	channel;

		void Play();
		bool IsPlaying();
		void Pause();
		bool IsPaused();
		void Stop();

		UINT GetLength();
		void SetPosition(UINT position);
		UINT GetPosition();
		void SetVolume(float volume);
		float GetVolume();
	};

	class SoundManager : public CSingleton<SoundManager>
	{
	public:
		SoundManager();
		~SoundManager();

		FMOD::System* system;
		vector<Sound> sounds;

		void Initialize();
		void Release();

		Sound LoadSound(_tstring path);

	private:
		Sound CreateSound(_tstring path);
	};
}