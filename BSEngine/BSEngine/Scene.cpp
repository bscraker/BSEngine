#include "Scene.h"

namespace BSEngine
{
	Scene::Scene(_tstring _name)
	{
		name = _name;
	}

	Scene::~Scene() {}

	void Scene::Update()
	{
		objectManager.Update();
	}

	void Scene::Render()
	{
		objectManager.Render();
	}

	void Scene::Release()
	{
		objectManager.Release();
	}
}