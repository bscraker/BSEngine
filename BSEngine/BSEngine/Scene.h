#pragma once

#include "ObjectManager.h"

namespace BSEngine
{
	class Scene
	{
	public:
		Scene(_tstring _name);
		virtual ~Scene();

		_tstring		name;
		ObjectManager	objectManager;

		virtual void Initialize() = 0;
		virtual void Changed(bool isVisible) {};
		virtual void Update();
		virtual void Render();
		virtual void Release();
	};
}