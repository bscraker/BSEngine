#pragma once

#include <iostream>
#include <Windows.h>
#include <time.h>
#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

// DirectX
#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

// FMOD
#include <fmod.hpp>
#pragma comment(lib, "fmod_vc.lib")

// BSEngine
#include "Engine.h"
#include "Utility/Calculate.h"
#include "Utility/Convert.h"
#include "Utility/tstring.h"

// Debug
#ifdef _DEBUG
#	define main() _tmain(int argc, TCHAR *argv[])
#	define LOG(text) _tcout << TEXT("[Tick:") << GetTickCount() << TEXT("]	") << text << endl
#else
#	define main() WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpszCmdLine, int nCmdShow)
#	define LOG(text)
#endif

// Key Code
#define VK_0 0x30
#define VK_1 0x31
#define VK_2 0x32
#define VK_3 0x33
#define VK_4 0x34
#define VK_5 0x35
#define VK_6 0x36
#define VK_7 0x37
#define VK_8 0x38
#define VK_9 0x39
#define VK_A 0x41
#define VK_B 0x42
#define VK_C 0x43
#define VK_D 0x44
#define VK_E 0x45
#define VK_F 0x46
#define VK_G 0x47
#define VK_H 0x48
#define VK_I 0x49
#define VK_J 0x4A
#define VK_K 0x4B
#define VK_L 0x4C
#define VK_M 0x4D
#define VK_N 0x4E
#define VK_O 0x4F
#define VK_P 0x50
#define VK_Q 0x51
#define VK_R 0x52
#define VK_S 0x53
#define VK_T 0x54
#define VK_U 0x55
#define VK_V 0x56
#define VK_W 0x57
#define VK_X 0x58
#define VK_Y 0x59
#define VK_Z 0x5A