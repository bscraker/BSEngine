#pragma once

#include "BSEngine.h"
#include "Singleton.h"
#include <dirent.h>

namespace BSEngine
{
	class FileManager : public CSingleton<FileManager>
	{
	public:
		FileManager();
		~FileManager();

		vector<_tstring> GetFileNames(_tstring path);

		void WriteText(_tstring path, _tstring text, bool append = false);
		_tstring ReadText(_tstring path);

	private:
		_tifstream ifStream;
		_tofstream ofStream;
	};
}