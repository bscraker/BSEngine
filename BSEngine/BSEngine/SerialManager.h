#pragma once

#include "BSEngine.h"
#include "Singleton.h"
#include "Serial.h"

namespace BSEngine
{
	struct Serial
	{
		_tstring portName;

		CSerial* serial;

		_tstring ReadString();
		void WriteString(_tstring data);
	};

	class SerialManager : public CSingleton<SerialManager>
	{
	public:
		SerialManager();
		~SerialManager();

		vector<Serial> serials;

		Serial OpenSerial(_tstring portName);

		void Release();

	private:
		Serial CreateSerial(_tstring portName);
	};
}