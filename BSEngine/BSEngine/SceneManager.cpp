#include "SceneManager.h"

namespace BSEngine
{
	SceneManager::SceneManager()
	{
		currentScene = NULL;
	}

	SceneManager::~SceneManager() {}

	void SceneManager::AddScene(Scene* _scene)
	{
		scenes.push_back(_scene);
		_scene->Initialize();
	}

	void SceneManager::ChangeScene(_tstring _name)
	{
		for each (Scene* scene in scenes)
		{
			if (scene->name == _name)
			{
				if (currentScene)
				{
					currentScene->Changed(false);
				}

				currentScene = scene;
				currentScene->Changed(true);
			}
		}
	}

	Scene* SceneManager::GetScene(_tstring _name)
	{
		for each (Scene* scene in scenes)
		{
			if (scene->name == _name)
			{
				return scene;
			}
		}

		return NULL;
	}

	void SceneManager::DeleteScene(_tstring _name)
	{
		if (currentScene->name != _name)
		{
			const int MAX = scenes.size();

			for (int i = 0; i < MAX; ++i)
			{
				if (scenes[i]->name == _name)
				{
					scenes[i]->Release();
					delete scenes[i];

					scenes.erase(scenes.begin() + i);

					break;
				}
			}
		}
	}

	void SceneManager::Update()
	{
		for each (Scene* scene in scenes)
		{
			scene->Update();
		}
	}

	void SceneManager::Render()
	{
		for each (Scene* scene in scenes)
		{
			scene->Render();
		}
	}

	void SceneManager::Release()
	{
		for each (Scene* scene in scenes)
		{
			scene->Release();
			delete scene;
		}

		scenes.clear();
	}
}