#include "Calculate.h"
#include "Object.h"

namespace BSEngine
{
	namespace Utility
	{
		bool isCollision(float x1, float y1, float width1, float height1, float x2, float y2, float width2, float height2)
		{
			if ((x2 + width2 <= x1 || x2 >= x1 + width1) ||
				(y2 + height2 <= y1 || y2 >= y1 + height1))
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		bool isCollision(Object* object1, Object* object2)
		{
			return isCollision(
				object1->position.x, object1->position.y, object1->size.x, object1->size.y,
				object2->position.x, object2->position.y, object2->size.x, object2->size.y);
		}

		bool isCollision(float x1, float y1, float x2, float y2, float width, float height)
		{
			return isCollision(
				x1, y1, 0.0f, 0.0f,
				x2, y2, width, height);
		}

		bool isCollision(float x1, float y1, Object* object2)
		{
			return isCollision(
				x1, y1, 0.0f, 0.0f,
				object2->position.x, object2->position.y, object2->size.x, object2->size.y);
		}

		bool isCollisionCircle(float x1, float y1, float radius1, float x2, float y2, float radius2)
		{
			float d1 = pow(x1 - x2, 2) + pow(y1 - y2, 2);
			float d2 = pow(radius1 + radius2, 2);

			if (d1 < d2)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		bool isCollisionCircle(float x1, float y1, float x2, float y2, float radius2)
		{
			return isCollisionCircle(
				x1, y1, 0,
				x2, y2, radius2);
		}

		void ComputeDistance(float distance, float angle, float* _x, float* _y)
		{
			*_x = distance * cos(angle);
			*_y = distance * sin(angle);
		}

		D3DXVECTOR2 ComputeVector2(float angle)
		{
			D3DXVECTOR2 vector = D3DXVECTOR2(cos(D3DXToRadian(angle)), sin(D3DXToRadian(angle)));
			D3DXVec2Normalize(&vector, &vector);

			return vector;

		}

		D3DXVECTOR2 ComputeVector2(float angle, float distance)
		{
			D3DXVECTOR2 vector = D3DXVECTOR2(cos(D3DXToRadian(angle)), sin(D3DXToRadian(angle)));
			D3DXVec2Normalize(&vector, &vector);

			return vector * distance;
		}
	}
}