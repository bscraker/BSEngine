#pragma once

#include <Windows.h>
#include <tchar.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

#ifdef _UNICODE
	typedef wstring _tstring;
	typedef wfstream _tfstream;
	typedef wofstream _tofstream;
	typedef wifstream _tifstream;
	typedef wostream _tostream;
	typedef wistream _tistream;
	typedef wstringstream _tstringstream;
#	define _tcout wcout
#	define _tcin wcin
#	define to_tstring to_wstring
#else
	typedef string _tstring;
	typedef fstream _tfstream;
	typedef ofstream _tofstream;
	typedef ifstream _tifstream;
	typedef ostream _tostream;
	typedef istream _tistream;
	typedef stringstream _tstringstream;
#	define _tcout cout
#	define _tcin cin
#	define to_tstring to_string
#endif