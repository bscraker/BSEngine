#pragma once

#include <Windows.h>
#include <string>
#include <atlconv.h>

using namespace std;

namespace BSEngine
{
	namespace Utility
	{
		char* ANSIToUTF8(const char* pszCode);
		char* UTF8ToANSI(const char* pszCode);

		wstring ConvertString(const string& _src);
		string ConvertString(const wstring& _src);
	}
}