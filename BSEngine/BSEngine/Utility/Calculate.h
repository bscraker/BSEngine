#pragma once

#include <d3d9.h>
#include <d3dx9.h>

namespace BSEngine
{
	class Object;
}

namespace BSEngine
{
	namespace Utility
	{
		bool isCollision(
			float x1, float y1, float width1, float height1,
			float x2, float y2, float width2, float height2);

		bool isCollision(
			Object* object1, Object* object2);

		bool isCollision(
			float x1, float y1,
			float x2, float y2, float width, float height);

		bool isCollision(
			float x1, float y1,
			Object* object);

		bool isCollisionCircle(
			float x1, float y1, float radius1,
			float x2, float y2, float radius2);

		bool isCollisionCircle(
			float x1, float y1,
			float x2, float y2, float radius2);

		void ComputeDistance(
			float distance, float angle,
			float* _x, float* _y);

		D3DXVECTOR2 ComputeVector2(
			float angle);

		D3DXVECTOR2 ComputeVector2(
			float angle, float distance);
	}
}