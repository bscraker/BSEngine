#include "FileManager.h"

namespace BSEngine
{
	FileManager::FileManager() {}

	FileManager::~FileManager() {}

	vector<_tstring> FileManager::GetFileNames(_tstring path)
	{
		vector<_tstring> fileNames;

		DIR *dir = opendir(
#ifdef _UNICODE
			Utility::ConvertString(path).c_str()
#else
			path.c_str()
#endif
			);

		if (dir)
		{
			struct dirent *ent;

			while (ent = readdir(dir))
			{
				fileNames.push_back(
#ifdef _UNICODE
					Utility::ConvertString(ent->d_name)
#else
					ent->d_name
#endif
					);
			}

			closedir(dir);
		}

		return fileNames;
	}

	void FileManager::WriteText(_tstring path, _tstring text, bool append)
	{
		if (append)
		{
			ofStream.open(path, ios::app);
		}
		else
		{
			ofStream.open(path);
		}

		ofStream << text;
		ofStream.close();
	}

	_tstring FileManager::ReadText(_tstring path)
	{
		ifStream.open(path);
		_tstring text((istreambuf_iterator<TCHAR>(ifStream)), istreambuf_iterator<TCHAR>());
		ifStream.close();

		return text;
		
	}

}