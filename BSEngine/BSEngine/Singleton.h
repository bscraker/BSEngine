#pragma once

template <class T>
class CSingleton
{
private:
	static T* instanse;

public:
	CSingleton() {};

	virtual ~CSingleton()
	{
		delete instanse;
	};

	static T* GetInstance()
	{
		if (instanse == NULL)
		{
			instanse = new T();
		}

		return instanse;
	};
};

template<class T>T* CSingleton<T>::instanse = 0;