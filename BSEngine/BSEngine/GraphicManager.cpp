#include "GraphicManager.h"

#include "../resource.h"

namespace BSEngine
{
	GraphicManager::GraphicManager()
	{
		device = NULL;
		d3d = NULL;
		sprite = NULL;
		font = NULL;
	}

	GraphicManager::~GraphicManager() {}

	LRESULT WINAPI WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		if (message == WM_DESTROY)
		{
			PostQuitMessage(0);
			return 0;
		}
		else
		{
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}

	void GraphicManager::Initialize(UINT width, UINT height, _tstring className, _tstring windowName, HICON icon /*= NULL*/, HCURSOR cursor /*= NULL*/, bool showCursor /*= true*/)
	{
		WNDCLASSEX wc =
		{
			sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0, 0,
			GetModuleHandle(NULL), icon, cursor, NULL, NULL,
			className.c_str(), icon
		};

		RegisterClassEx(&wc);

		int style = WS_OVERLAPPEDWINDOW - WS_THICKFRAME - WS_MAXIMIZEBOX;

		RECT rect = {0, 0, width, height};
		AdjustWindowRect(&rect, style, FALSE);

		hWnd = CreateWindow(className.c_str(), windowName.c_str(),
			style,
			(GetSystemMetrics(SM_CXSCREEN) - (rect.right - rect.left)) / 2,
			(GetSystemMetrics(SM_CYSCREEN) - (rect.bottom - rect.top)) / 2,
			rect.right - rect.left,
			rect.bottom - rect.top,
			NULL, NULL, wc.hInstance, NULL);

		ShowWindow(hWnd, SW_SHOWDEFAULT);
		UpdateWindow(hWnd);

		d3d = Direct3DCreate9(D3D_SDK_VERSION);
		
		D3DPRESENT_PARAMETERS d3dpp;
		ZeroMemory(&d3dpp, sizeof(d3dpp));

		d3dpp.Windowed = true;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		d3dpp.BackBufferWidth = width;
		d3dpp.BackBufferHeight = height;
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

		d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
			D3DCREATE_SOFTWARE_VERTEXPROCESSING,
			&d3dpp, &device);

		ShowCursor(showCursor);

		CreateSprite();
		CreateFont();
	}

	void GraphicManager::Release()
	{
		if (device)
		{
			device->Release();
		}

		if (d3d)
		{
			d3d->Release();
		}

		if (sprite)
		{
			sprite->Release();
		}

		if (font)
		{
			font->Release();
		}
	}

	void GraphicManager::CreateSprite()
	{
		D3DXCreateSprite(device, &sprite);
	}

	void GraphicManager::CreateFont()
	{
		D3DXCreateFont(device, 30, 0, FW_BOLD, 1, false, DEFAULT_CHARSET, NULL, NULL, NULL, TEXT("��������ü"), &font);
	}

	void GraphicManager::RenderObject(Object* object)
	{
		if (!object || !object->visible)
		{
			return;
		}

		float angle = D3DXToRadian(object->angle);

		D3DXMATRIX matrix;
		D3DXMATRIX scaling;
		D3DXMATRIX translation;
		D3DXMATRIX rotation;
		D3DXMATRIX translation2;

		D3DXMatrixIdentity(&matrix);
		D3DXMatrixIdentity(&scaling);
		D3DXMatrixIdentity(&translation);
		D3DXMatrixIdentity(&rotation);
		D3DXMatrixIdentity(&translation2);

		D3DXMatrixScaling(&scaling, object->size.x / object->texture.info.Width, object->size.y / object->texture.info.Height, 1.0f);
		D3DXMatrixTranslation(&translation, -object->centerPosition.x, -object->centerPosition.y, 0);
		D3DXMatrixRotationZ(&rotation, angle);
		D3DXMatrixTranslation(&translation2, object->centerPosition.x + object->position.x, object->centerPosition.y + object->position.y, 0);

		matrix = scaling * translation * rotation * translation2;
		
		sprite->SetTransform(&matrix);
		sprite->Begin(D3DXSPRITE_ALPHABLEND);
		sprite->Draw(object->texture.texture, NULL, NULL, NULL, object->color);
		sprite->End();
	}

	void GraphicManager::RenderText(_tstring text, int x, int y, float scale, float _angle, D3DXCOLOR color)
	{
		if (text.empty())
		{
			return;
		}

		float angle = D3DXToRadian(_angle);

		D3DXMATRIX matrix;
		D3DXMATRIX scaling;
		D3DXMATRIX rotation;
		D3DXMATRIX translation;

		D3DXMatrixIdentity(&matrix);
		D3DXMatrixIdentity(&scaling);
		D3DXMatrixIdentity(&rotation);
		D3DXMatrixIdentity(&translation);

		D3DXMatrixScaling(&scaling, scale, scale, 1.0f);
		D3DXMatrixRotationZ(&rotation, angle);
		D3DXMatrixTranslation(&translation, (float)x, (float)y, 0.0f);

		matrix = scaling * rotation * translation;

		sprite->SetTransform(&matrix);

		sprite->Begin(D3DXSPRITE_ALPHABLEND);
		font->DrawText(sprite, text.c_str(), -1, NULL, DT_NOCLIP, color);
		sprite->End();
	}

	void GraphicManager::RenderStart()
	{
		device->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
		device->BeginScene();
	}

	void GraphicManager::RenderEnd()
	{
		device->EndScene();
		device->Present(NULL, NULL, NULL, NULL);
	}
}